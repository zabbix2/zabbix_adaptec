#!/bin/bash

# Variables
workdir=/opt/scripts/zabbix/adaptec
resultdir=$workdir/result
zbxdiscovery_arrays=$resultdir/zbxdiscovery_arrays
zbxdiscovery_lv=$resultdir/zbxdiscovery_lv
zbxdiscovery_sensors=$resultdir/zbxdiscovery_sensors

# Begin discovery file
mkdir -p $resultdir
echo "{\"data\":[" > $zbxdiscovery_arrays
echo "{\"data\":[" > $zbxdiscovery_lv
echo "{\"data\":[" > $zbxdiscovery_sensors

# Get info about Controller
config=`/usr/local/bin/arcconf GETCONFIG 1`

# Get ControllerModel
echo "${config}" | grep "Controller Model" | sed -r 's|(.*)(:.*)|\2|' | sed 's/: //g' > $resultdir/controller_model

# Get ControllerStatus
echo "${config}" | grep "Controller Status" | grep Optimal | wc -l > $resultdir/controller_status

# Get Temperature status
echo "${config}" | grep "Temperature" | grep "(Normal)" | wc -l > $resultdir/temperature_status

# Get Cache Status
echo "${config}" | grep "Cache Status" | grep Ok | wc -l > $resultdir/cache_status

# Battery status
echo "${config}" | grep "Backup Power Status" | grep Ok | wc -l > $resultdir/battery_status

# Get SMART
/usr/local/bin/arcconf GETSMARTSTATS 1 |grep "SMART Health Status" | grep -v Passed | wc -l > $resultdir/smart_nopassed_count

# Get Errors counters
echo "${config}" | grep "Errors" | grep -v "Recovered" | sed 's/[^0-9|,]//g' | grep -v "^0" | wc -l > $resultdir/errors_in_counters

# Get Arrays
for array in `echo "${config}" | grep "Array Number" | awk '{print $3}'`; do
  echo "{ \"{#ADAPTECARRAY}\":\"$array\" }, " >> $zbxdiscovery_arrays
  /usr/local/bin/arcconf GETCONFIG 1 AR $array | grep Status | grep Ok | wc -l > $resultdir/array_status_${array}
done

# Get Logical Drives
for lv in `echo "${config}" | grep "Logical Device number" | awk '{print $4}'`; do
  echo "{ \"{#ADAPTECLOGICALVOLUME}\":\"$lv\" }, " >> $zbxdiscovery_lv
  /usr/local/bin/arcconf GETCONFIG 1 LD $lv | grep "Logical Device name" | sed -r 's|(.*)(:.*)|\2|' |sed 's/[ |:]//g' > $resultdir/lv_name_${lv}
  /usr/local/bin/arcconf GETCONFIG 1 LD $lv | grep "Status of Logical Device" | grep Optimal | wc -l > $resultdir/lv_status_${lv}
  /usr/local/bin/arcconf GETCONFIG 1 LD $lv | grep "RAID level" | sed 's/[^0-9]//g' > $resultdir/lv_raid_level_${lv}
done

# Get Current Temperature of disks
sensors_data=`echo "${config}" | grep "Current Temperature" | sed 's/[^0-9]//g' | awk '{printf "sensor%s %s\n", NR, $0}'`
for sensor in `echo "${sensors_data}" | awk '{print $1}'`; do
  echo "{ \"{#ADAPTECSENSOR}\":\"${sensor}\" }, " >> $zbxdiscovery_sensors
  sensor_value=`echo "${sensors_data}" | grep $sensor | awk '{print $2}'`
  echo $sensor_value > $resultdir/$sensor
done

# Finish discovery file
echo "]}" >> $zbxdiscovery_arrays
echo "]}" >> $zbxdiscovery_lv
echo "]}" >> $zbxdiscovery_sensors
zbxdiscovery_arrays_tmp=`cat $zbxdiscovery_arrays |tr -d '\r\n' |sed 's/\(.*\),/\1/'`
zbxdiscovery_lv_tmp=`cat $zbxdiscovery_lv |tr -d '\r\n' |sed 's/\(.*\),/\1/'`
zbxdiscovery_sensors_tmp=`cat $zbxdiscovery_sensors |tr -d '\r\n' |sed 's/\(.*\),/\1/'`
echo $zbxdiscovery_arrays_tmp > $zbxdiscovery_arrays
echo $zbxdiscovery_lv_tmp > $zbxdiscovery_lv
echo $zbxdiscovery_sensors_tmp > $zbxdiscovery_sensors
